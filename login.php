<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <title>Dia del niño</title>
</head>

<body style="width: 100%; height: 100%; background: url('./assets/login.png'); background-size: cover">
  <img src="./assets/logo_bp.png" style="position: fixed; top: 0; right: 63px" width="270" class="brand_logo" />
  <img src="./assets/logo_ac.png" style="position: fixed; bottom: 130px; left: 0" width="114" class="logo_ac" />
  <form id="login" class="pt-5 text-center">
    <img src="./assets/children.png" style="margin: 0 auto" class="login-img" />
    <p class="text-center" style="color: #595D6E; font-size: 18px; margin-bottom: 34px; max-width: 350px; margin: 0 auto 34px">Si aún no te has registrado, haz clic en registro. Si ya estás registrado, ingresa con tus datos aquí.</p>
    <div class="container">
      <div id="login-row" class="row justify-content-center align-items-center">
        <div id="login-box" class="col-md-12" style="max-width: 330px; margin-bottom: 50px">
          <form id="login-form" class="form" action="" method="post">
            <div class="form-group">
              <input type="email" name="email" placeholder="Correo Electrónico" id="email" class="form-control">
            </div>
            <div class="form-group" style="margin-bottom: 30px">
              <input type="password" name="password" placeholder="Número Cédula" id="password" class="form-control">
            </div>
            <div class="text-center">
              <a href="javascript:void(0)" id="loginSend" class="btn btn-primary" style="padding-left: 2em; padding-right: 2em; text-align: center; width: 100%">INGRESAR</a>
            </div>
            <div class="text-center">
              <div style="color: #7B7F92; font-size: 14px; margin-top: 30px; margin-bottom: 12px" class="text-center">¿No te has registrado?</div>
              <a href="/registro.php" class="btn btn-secondary-alt" style="padding-left: 2em; padding-right: 2em; text-align: center; width: 100%">REGISTRO</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </form>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

  <script>
   

    $('#loginSend').on('click', function(event) {

      event.preventDefault();
      if ($("#password").val() == '' || $("#username").val() == '') {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Debes ingresar el usuario y contraseña'
        })
        return false;
      }


      var formData = new FormData();
      formData.append('email', $('#email').val());
      formData.append('password', $('#password').val());

      $.ajax({
        type: 'POST',
        url: 'https://api.siempreconectados.com/api/login',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $("#loginSend").text('INICIANDO SESIÓN...');
        },
        success: function(msg) {

          $.each(msg.data, function(index, value) {
            if (index == 'user') {
              localStorage.setItem(index, JSON.stringify(value));
            } else if (index == 'access_token') {
              localStorage.setItem('token', value);
            } else {
              localStorage.setItem(index, value);
            }

          });
          
          localStorage.setItem('son_id', '');          
          localStorage.setItem('activity_id', 1); 
          localStorage.setItem('base_url', 'https://api.siempreconectados.com');
          


          var jqXhr = $.ajax({
            type: 'POST',
            url: 'include/setCookie.php',
            data: {
              "cookie": msg.data.access_token
            },
            dataType: 'json'
          });

         
          setTimeout(function(){  window.location.replace('sons.php'); }, 3000);



        },
        error: function(jqXHR) {
          console.log(jqXHR.responseJSON, jqXHR.status);

          
          if(jqXHR.status == 401){
                    response = "La combinación de correo electronico y cédula es incorrecta"
                    icon_item = 'warning';
                    title_item = 'Inicio de sesión inválido';
                }else{
                    response = "Ha ocurrido un error, intenta nuevamente más tarde"
                    icon_item = 'error';
                    title_item = 'Oops...';
                }

          //response = "Ha ocurrido un error, intenta nuevamente mas tarde";
          Swal.fire({
            icon: icon_item,
            title: title_item,
            text: response
          })

          $("#loginSend").text('INGRESAR');

          return false;
        }
      });
    });

    /*
        $('form').on('submit', function(event) {
          event.preventDefault();
          fetch('https://api.siempreconectados.com/api/login', {
            method: 'post',
            body: JSON.stringify({
              email: $('[name="email"]').val(),
              password: $('[name="password"]').val(),
            })
          })
            .then((res) => {
              return res.json()
            })
            .then((res) => {
              setCookie('token', res.data.access_token, 1)
               localStorage.setItem('token', res.data.access_token)
               window.location.href = "/"
            })
        })
      */
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>

</html>