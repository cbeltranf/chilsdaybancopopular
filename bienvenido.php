<?php
  /* ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL); */
  include('./include/utils.php');
  $result = callAPI('get', 'https://api.siempreconectados.com/api/activities');
  $activities = json_decode($result);  
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-IIED/eyOkM6ihtOiQsX2zizxFBphgnv1zbe1bKA+njdFzkr6cDNy16jfIKWu4FNH" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/lastActivity.css">
  <link rel="stylesheet" href="./css/aCalendar.css">
  <title>Bienvenido</title>
</head>

<body>
  <?php include('./include/menu.php') ?>
  <!-- PageContent -->
  <div>
    <?php include('./include/lastActivity.php') ?>
    <?php include('./include/activityCalendar.php') ?>
  </div>
  <!-- /PageContent -->

  <div class="modal" tabindex="-1" role="dialog" id="modalauto">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content p-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 20px; right: 20px; z-index: 99999">
            <div class="fal fa-times" style="font-size: 38px; color: #707070;"></div>
          </button>
          <div class="owl-carousel owl-in-modal">
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">BIENVENIDO(A)</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  ¡Hemos creado un lugar especial para ti, en donde podrás divertirte, jugar, interactuar y ganar premios!
                </p>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Participa activamente en cada actividad y en nuestra comunidad, comentando y dando me gusta entre el 25 y 1 de mayo de 2020.
                </p>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">ACTIVIDADES</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Cada día tendrás diferentes retos y actividades para que realices en casa. Para participar, solo haz clic en el botón:
                </p>
                <button class="btn btn-primary w-100 btn-third" data-toggle="modal" data-target="#modalauto">COMENZAR ACTIVIDAD</button>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"  crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <?php include('me.php');?>
  <script>
    $(document).ready(function(){
      $('#modalauto').modal('toggle');
      $(".owl-carousel").owlCarousel({
        items: 1,
        dots: true,
        nav: true
      });
    });
    $( document ).ready(function() {
        moment.locale('es');
        var token = localStorage.getItem('token');
        var son_id = localStorage.getItem('son_id');

        if (!token) {
            location.href = '/'
        }else{
          if(!son_id){
              location.href = '/sons.php'
          } 
        }    
        
        $("#clear_sons").on("click", function(){
          localStorage.removeItem('son_id');
          location.href = '/'
        });
    });

    $(() => {
      $('.moment-tf').each(function(index)  {
        var data = $(this).data('date');
        $(this).text(moment(data).format('dddd D MMMM'))
      })
    })
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>
</html>