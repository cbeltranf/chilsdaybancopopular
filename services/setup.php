<?php
class ItMysqli extends mysqli {
  public function __construct($host, $usuario, $contraseña, $bd) {
    parent::init();

    if (!parent::options(MYSQLI_INIT_COMMAND, 'SET AUTOCOMMIT = 0')) {
        die('Falló la configuración de MYSQLI_INIT_COMMAND');
    }

    if (!parent::options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)) {
        die('Falló la configuración de MYSQLI_OPT_CONNECT_TIMEOUT');
    }

    if (!parent::real_connect($host, $usuario, $contraseña, $bd)) {
        die('Error de conexión (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
    }
  }
}


class ItDbCon {
  // public $con = new ItMysqli('localhost', 'root', 'root', 'childday');
}

