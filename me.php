<script>
    check_me();

    function check_me() {
        var jqXhr = $.ajax({
            type: 'GET',
            url: 'https://api.siempreconectados.com/api/me',
            dataType: 'json',
            beforeSend: function(xhr) {
                var tok = localStorage.getItem('token');
                xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
            },
            success: function(msg) {
                if (msg.data.activityId.id) {
                    localStorage.setItem('activity_id', msg.data.activityId.id);
                }

            },
            error: function(jqXHR) {
                if (jqXHR.status == 401) {
                    localStorage.clear();
                    Swal.fire({
                        icon: "warning",
                        title: "Tu sesión ha expirado",
                        text: "Inicia sesión nuevamente"
                    }).then((result) => {
                        window.location.replace('login.php');
                    })
                    return false;
                }
            }

        });
    }
</script>
<style>
.swal2-icon.swal2-warning {
    border-color: #3085d6 !important;
    color: #3085d6 !important;
    width: 65px !important;
    height: 65px !important;
}
.swal2-styled.swal2-confirm{
	font-size: 1em !important;
}
</style>
<script type="text/javascript">
    var timeoutID;
    var killingID;
    var alert;

    function setup() {
        this.addEventListener("mousemove", resetTimer, false);
        this.addEventListener("mousedown", resetTimer, false);
        this.addEventListener("keypress", resetTimer, false);
        this.addEventListener("DOMMouseScroll", resetTimer, false);
        this.addEventListener("mousewheel", resetTimer, false);
        this.addEventListener("touchmove", resetTimer, false);
        this.addEventListener("MSPointerMove", resetTimer, false);

        startTimer();
    }
    setup();

    function startTimer() {
        // wait 2 seconds before calling goInactive
        //console.log('start');
        timeoutID = window.setTimeout(goInactive, 480000);
    }

    function resetTimer(e) {
        //console.log("Resurrect");
        window.clearTimeout(timeoutID);
        window.clearTimeout(killingID);
        Swal.close()
        goActive();
    }

    function kill_session() {
        killingID = setTimeout(() => {
            var time = parseInt($("#timeout").text());
            console.log(time);

            time--;
            if (time < 1) {
                $("#bye").empty().append("Cerrado sesión...");
                console.log("bye man :(");
                localStorage.clear();
                window.location.replace("login.php");                
                return false;
            } else {
                if (time < 3) {
                    $("#bye").css('font-weight', 'bold').css('color', '#900');
                }
                $("#timeout").text(time);
                kill_session();
            }
        }, 1200);
    }

    function goInactive() {
        // do something
        kill_session();
        alert = Swal.fire({
            title: '¿Aún estás allí?',
            html: "<span id='bye' style='font-size:1.8rem'>Por seguridad tu sesión se cerrará en <span style='font-weight:bold; color:#900' id='timeout'>120</span> Segundos.</span>",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Permanecer en esta sesión',
            onClose: () => {
                resetTimer(this);
            }
        }).then((result) => {
            if (result.value) {
                goActive();
            }
        })

        //console.log("Bye");
    }

    function goActive() {
        // do something				
        //console.log("Resurrect");
        startTimer();
    }
</script>