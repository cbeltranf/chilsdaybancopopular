<!DOCTYPE html>
<html lang="es">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css"
      integrity="sha384-IIED/eyOkM6ihtOiQsX2zizxFBphgnv1zbe1bKA+njdFzkr6cDNy16jfIKWu4FNH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">
    <link rel="stylesheet" href="./css/theme.css">
    <title>Dia del niño</title>
  </head>

  <body style="width: 100%; height: 100%; background: url('./assets/login.png'); background-size: cover">
    <div id="app">
      <img src="./assets/logo_bp.png" style="position: fixed; top: 0; right: 63px" width="270" class="brand_logo" />
      <img src="./assets/logo_ac.png" style="position: fixed; bottom: 130px; left: 0" width="114" class="logo_ac" />
      <div id="login" class="pt-5">
        <h3 class="text-center" style="color: #595D6E; font-size: 21px">REGISTRO</h3>
        <p style="font-size: 18px; color: #595D6E; max-width: 380px; margin: 20px auto 40px" class="text-center">
          ¡Regístrate para que tú y tu familia hagan parte de la celebración!
        </p>
        <div class="container" id="">
          <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-box" class="col-md-12" style="max-width: 330px; margin-bottom: 50px">
              <form v-on:submit.prevent="submitForm">
                <div class="form-group">
                  <label for="name"><i class="fas fa-user text-secondary"></i> Nombre del Trabajador</label>
                  <input type="text" id="name" name="name" placeholder="Nombre Completo" v-model="worker_name"
                    class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="name"><i class="fal fa-id-card text-secondary"></i> Número Cédula</label>
                  <input v-model="worker_id" type="number" id="name" name="name" max="99999999999" placeholder="Número de Cédula"
                    class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="email"><i class="fal fa-envelope text-secondary"></i> Correo Electrónico </label>
                  <input type="email" name="email" placeholder="Correo Electrónico" id="username" class="form-control"
                    required v-model="worker_email">
                </div>
                <div class="form-group">
                  <a class="text-primary" style="font-size: 12px" data-toggle="modal" data-target="#exampleModal"><i class="fal fa-child"></i>
                    Agregar Hijo</a>
                </div>
                <div class="form-group">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                    <small>Autorizó la publicación de las fotos que se suban a este portal por medio de mi usuario.  <br>Este contenido será utilizado exclusivamente para fines, de nuestras actividades corporativas, Uso exclusivo para trabajadores del Banco Popular.</small>
                    </label>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary"
                    style="padding-left: 2em; padding-right: 2em; text-align: center; width: 100%">REGISTRAR</button>
                </div>
                <div class="text-center">
                  <div style="color: #7B7F92; font-size: 14px; margin-top: 30px; margin-bottom: 12px"
                    class="text-center">¿Ya estás registrado? <a href="/login.php">Ingresar</a></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body text-center">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title text-primary mb-3">Agregar Hijo</h4>
              <p class="mb-5">Agrega el nombre de tu hijo para crear su perfil y pueda acceder</p>
              <form class="row justify-content-center" v-on:submit.prevent="submitChild">
                <div class="col-8 d-flex" v-for="(child, index) in childrens">
                  <div class="mr-3"
                    style="border-radius: 50%; background-image: url('./assets/child.png'); width: 52px; height: 52px; background-size: cover;">
                  </div>
                  <div class="form-group" style="flex: 1">
                    <label for="">Nombre</label>
                    <input type="text" name="child_name" placeholder="Nombre" id="username" class="form-control"
                      v-model="child.name" required>
                  </div>
                  <a v-on:click="remove(index)" v-if="index > 0"
                    style="font-size: 20px; margin-top: 20px; margin-right: 10px; cursor: pointer">&times;</a>
                </div>
                <div class="col-8 d-block text-left mb-3">
                  <a class="text-primary" v-on:click="childrens.push({ name: '' })">Agregar otro hijo</a>
                </div>
                <div class="col-8">
                  <button type="submit" class="btn btn-primary w-100">AGREGAR</button>
                </div>
            </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal" tabindex="-1" role="dialog" id="modalauto">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content p-3">
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Queremos celebrar con todos tus niños, por eso, es importante registrarlos. 
                  Cada niño quedará con un perfil diferente, el cual será su acceso cada vez que quiera ingresar a la página web.
                </p>
                <button class="btn btn-primary w-100" data-toggle="modal" data-target="#modalauto">ENTENDIDO</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" tabindex="-1" role="dialog" id="signing-complete">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content p-3">
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <p class="text-center" style="font-size: 14px; color: #000000">
                  ¡Gracias por registrarte!
                </p>
                <button type="button" class="btn btn-primary w-100" v-on:click="login()">ENTENDIDO</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
      crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
      function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }
    </script>
    <script>

      $(() => {
        $('#modalauto').modal('toggle')
      })

      var app = new Vue({
        el: '#app',
        data: {
          demo: 'demo',
          worker_name: '',
          worker_id: null,
          worker_email: '',
          childrens: [{
            name: ''
          }]
        },
        methods: {
          remove: function (index) {
            this.childrens.splice(index, 1);
          },
          submitChild: function (event) {
            event.preventDefault();
            $('#exampleModal').modal('toggle')
          },
          login: function () {
            console.log('pasaz')
            fetch('https://api.siempreconectados.com/api/login', {
              method: 'post',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
              body: JSON.stringify({
                email: this.worker_email,
                password: this.worker_id
              })
            })
              .then((res) => res.json())
              .then((res) => {
                setCookie('token', res.data.access_token, 1)
                localStorage.setItem('token', res.data.access_token)
                window.location.href = "/"
              })
          },
          submitForm: function (event) {
            event.preventDefault();
            const sons = this.childrens.map(c => c.name);

            if (sons.indexOf('') > -1) {
              swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Debe especificar los nombres de los niños participantes',
              })
              return;
            }
            fetch('https://api.siempreconectados.com/api/register', {
              method: 'post',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
              body: JSON.stringify({
                "name": this.worker_name,
                "email": this.worker_email,
                "password": this.worker_id,
                "document": this.worker_id,
                "family": "default",
                "roles": "T",
                "sons": sons
              })
            })
              .then(res => {
                if (res.ok) {
                  return res.json()
                }

                if (res.status == 422) {
                  return res.json().then((jres) => {
                    var errorData = jres.data;
                    if (errorData.document) {
                      swal.fire({
                        icon: 'error',
                        title: 'Error...',
                        text: 'El documento ya se encuentra registrado',
                      })
                    }
                    else if (errorData.email) {
                      swal.fire({
                        icon: 'error',
                        title: 'Error...',
                        text: 'El correo ya se encuentra registrado',
                      })
                    } else {
                      throw new Error();
                    }
                    
                    return {
                      error: true
                    }
                  })
                }
                throw new Error();
              })
              .then(res => {
                if (res.error) {
                  return;
                }
                $('#signing-complete').modal('toggle');
              })
              .catch(ex => {
                console.error(ex)
                swal.fire({
                  icon: 'error',
                  title: 'Error...',
                  text: 'Fallo la conexión al servidor!',
                })
              })
          }
        }
      })
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

  </body>

</html>