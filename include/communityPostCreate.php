<style>
  .card {
    border-radius: .90rem;

  }

  .card-header {
    background: #F5F6F7 0% 0% no-repeat padding-box;
    border-color: #DEDFE2;
    color: #333;
    font-weight: bold;
    padding: 10px 20px 10px 20px;
    border-top-left-radius: .80rem !important;
    border-top-right-radius: .80rem !important;
  }

  .my-badge {
    background-color: #94BE32;
    border-radius: .80rem;
    padding: 12px;
  }

  #newpost{
    color:#90949C;
    width: 100%;
    overflow: hidden;
    border: 1px solid #EBEBEB;
    max-height: 3.1rem;
    font-size: 1rem;
    border-radius: 5px;
  }
  .new-post-controls{ 
  
  border:         none;
  border-left:    1px solid #E2E2E2;
   
}

.new-post-controls i{ 
 color:#94BE32;
 font-size: 25px;
 margin-left:7px;
 margin-top: auto;
 margin-bottom: auto;
 display:inline-block;
 text-align: center;
 vertical-align: bottom;
 cursor: pointer;
}

</style>
<form id="newpostform" action="">
<div class=" container text-center mt-3">
  <div class="row justify-content-md-center">
    <div class="col-md-8 col-sm-12 col-lg-8">
      <div class="card my-card">
        <div class="card-header text-left">
          Crear publicación
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-2 col-md-12 col-sm-12 text-center my-auto">
              <div class="d-flex">
                <img src="//api.siempreconectados.com/media/sons/default.svg" class="avatar" />
              </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
              <textarea name="newpost" id="newpost" placeholder="Cuéntanos algo sobre tu experiencia con la actividad (este campo es obligatorio)" ></textarea>
              <input  accept="image/x-png,image/gif,image/jpeg"  type="file" name="post_image" id="post_image" style="display:none">
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 new-post-controls text-center align-middle  my-auto">
            <i class="far fa-camera mt-2 mx-auto" id="add_photo"></i> &nbsp; <i class="fas fa-paper-plane  mt-2 mx-auto" id="send_post"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>