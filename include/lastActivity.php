<?php
$date = date("d.m.Y");
$lasta = null;
foreach($activities->data as $activity) {
  $match_date = date('d.m.Y', strtotime($activity->date_release));
  if ($match_date == $date) {
    $lasta = $activity;
  }
}


?>
<div class="container">
  <div class="text-center">
    <div class="lastActivityLabel">
      <span style="color: rgba(112, 112, 112, 1); margin-right: 7px; font-size: 16px" class="moment-tf"><?= $lasta->date_release ?></span>
      <i class="far fa-calendar-check" style="color: <?= $lasta->category->color ?>; font-size: 18"></i>
    </div>
  </div>
  <div class="lastActivity__container">
    <div class="lastActivity__valueContainer text-center">
      <div class="lastActivity__valueTitle" style="background: <?= $lasta->category->color ?>">
        <?= $lasta->category->name ?>
      </div>
      <img src="https://api.siempreconectados.com/media/category/<?= $lasta->category->photo ?>" class="my-5" width="86" />
      <div class="lastActivity__valueSubTitle" style="color: <?= $lasta->category->color ?>">
        <?= $lasta->tittle ?>
      </div>
      <div class="lastActivity__valueText mt-2">
        <?= 
        $lasta->content
        ?>
      </div>
    </div>
    <div class="lastActivity__bannerContaienr" style="background-image: url(https://api.siempreconectados.com/media/activities/<?= $lasta->photo ?>)">
      <div class="lastActivity__bannerTitle" style="color: <?= $lasta->category->color ?>">
        <?= $lasta->tittle ?>
      </div>
      <div class="lastActivity__bannerMiddle">
        <div style="font-weight: bold; font-size: 33px; line-height: 40px"><?= $lasta->subtitle ?></div>
        <div style="font-size: 33px; line-height: 40px; max-width: 80%; margin: 0 auto"><?= $lasta->tittle ?></div>
      </div>
      <a class="lastActivity__footer" style="background-color: <?= $lasta->category->color ?>" href="/detalle.php?id=<?= $lasta->id ?>">
        EMPEZAR ACTIVIDAD
      </a>
    </div>
  </div>
</div>