<?php
$activities = $activities->data;
  $items = array_map(function($act) {
    return [
      'value' => $act->category->name,
      'color' => $act->category->color,
      'date' => $act->date_release,
      'title' => $act->tittle,
      'id' => $act->id,
      'image' => 'https://api.siempreconectados.com/media/category/' . $act->category->photo,
      'image_in_body' => 'https://api.siempreconectados.com/media/category/' . $act->category->photo,
      'footer_image' => 'https://api.siempreconectados.com/media/category/' . $act->category->img,
      'background_image' => 'https://api.siempreconectados.com/media/activities/' . $act->photo,
    ];
  }, $activities);
?>

<div class="container">
  <div class="row justify-content-center mt-5">
    <div class="col-md-10 col-12">
        <h5 class="text-center mb-2">ACTIVIDADES</h5>
        <p class="text-center mb-4" style="color: #707070; font-size: 14px">Diariamente  estaremos compartiendo diferentes actividades para realizar en familia.<br>
Recuerda que las actividades que realices del 25 al 30 de abril 2020 te permitirán ganar premios.</p>
    </div>
  </div>
  <div class="row">
    <?php foreach($items as $index => $item) {
      $isSmall = $index % 2 != 0;
      $isSecondLine = $index > 3;
      $isSmallLargeFile = $isSecondLine && $isSmall;
      $blured = new DateTime() < new DateTime($item['date']);
    ?>
      <div class="col-12 col-lg-3">
        <div class="acalendar__container <?= $isSecondLine ? 'second_line' : '' ?> <?= $isSmall ? 'isSmall' : '' ?>">
          <div class="d-flex justify-content-between pl-2 pr-2 pt-1 pb-2" style="font-size: 12px">
            <img src="<?= $item['image']  ?>" alt="" width="60" height="76" style="object-fit: cover">
            <div class="d-flex justify-content-center align-flex-start flex-column">
              <div class="text-white pl-2 pr-2 pt-1 pb-1 text-center" style="border-radius: 5px; background-color: <?= $item['color'] ?>"><?= $item['value'] ?></div>
              <div style="font-size: 14px" class="moment-tf" data-date="<?= $item['date'] ?>"></div>
            </div>
          </div>
          <div class="acalendar_body_image_container <?= $blured ? 'blured' : '' ?>">
            <img src="<?= $item['background_image'] ?>" class="acalendar_body_image"/>
            <?= !$blured ? '<a href="/detalle.php?id=' . $item['id'] . '" class="btn-start-activity" style="background-color: ' . $item['color']. '"><i class="fal fa-play" style="margin-right: 10px"></i>Empezar Actividad</a>' : ''?>
            <img src="<?= $item['image_in_body'] ?>" style="position: absolute; top: 50%; left: 50%; transform: translateY(-50%) translateX(-50%); z-index: 99; width: 70px " >
          </div>
          <div style="font-size: 14px; color: white; padding-top: 21px; padding-bottom: 21px; background-image: url('<?= $item['footer_image']; ?>'); background-size: cover" class="text-center">
            <?= $item['title'] ?>
          </div>
        </div>
      </div>
    <?php }?>
  </div>
  <!-- <div class="text-center" style="margin-top: -50px">
    <i class="fal fa-angle-down" style="color: #707070; font-size: 40px"></i>
  </div> -->
</div>
<footer style="background: black; width: 100%; height: 60px; position: absolute; left: 0; right: 0"></footer>