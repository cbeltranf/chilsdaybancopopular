<?php
$sons = $sons->data;
  $items = array_map(function($son) {
    return [

      'id' => $son->id,
      'name' => $son->name,
      'avatar' => 'https://api.siempreconectados.com/media/' . $son->avatar,
    ];
  }, $sons);

  
?>
<?php foreach($items as $index => $item) {?>
    <div class="sons__header-img btn_sons" id="<?php echo $item['id'];?>">
         <img class="sons__avatar" src="<?php echo $item['avatar'];?>"/>
         <p><?php echo $item['name']?></p>
      </div>
<?php }?>