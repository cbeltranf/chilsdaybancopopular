<!-- Nav-->
<script>
  localStorage.setItem('base_url', 'https://api.siempreconectados.com')
  localStorage.setItem('activity_id', '2')
  localStorage.setItem('token_type', 'bearer')

  setTimeout(() => {
    $('#logout').on('click', function() {
      console.log('pasa')
      localStorage.clear();
      location.href ="/login.php" 
    })
  }, 2000);
</script>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">

    <a class="navbar-brand" href="/">
      <img src="../assets/brand.png" height="100%" style="margin-bottom" class="brand" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="/bienvenido.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/comunidad.php">Comunidad</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/ranking.php">Ranking</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="validar__avatar" src="//api.siempreconectados.com/media/sons/default.svg" />
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/sons.php">Cambiar de usuario</a>
            <a class="dropdown-item" style="cursor: pointer" id="logout">Salir</a>
          </div>
        </li>
        </li>
        <li class="nav-item nav-siempre-conectados">
          <a href="https://siempreconectados.com/">
          <img src="/assets/SiempreConectados.png" /></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- /Nav-->