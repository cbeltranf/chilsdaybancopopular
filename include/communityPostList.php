<style>
  .postlist .card {
    border-radius: .60rem;

  }

  .postlist .card-header {
    background: #FFFFFF 0% 0% no-repeat padding-box;
    border-color: #DEDFE2;
    color: #333;
    font-weight: bold;
    padding: 10px 20px 5px 20px;
    border-top-left-radius: .60rem !important;
    border-top-right-radius: .60rem !important;
  }

  .postlist .my-badge {
    background-color: #94BE32;
    border-radius: .80rem;
    padding: 12px;
  }

  .postlist .card-img-top {
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
  }

  .postlist .card-body {
    font-size: 0.7rem;
  }

  .newcomment {
    color: #90949C;
    width: 100%;
    overflow: hidden;
    border: 1px solid #EBEBEB;
    max-height: 2.3rem;
    border-radius: 15px;
    padding: 7px 5px 1px 9px;
  }

  .interactions i {
    color: #94BE32;
    font-size: 20px;
    margin-right: 17px;
    cursor: pointer;
  }

  .showcomments {
    color: #797979;
  }

  .showcomments:hover {
    color: #707070;
  }

  .feedback__btn {
    position: absolute;
    bottom: 5px;
    right: 27px;
    padding: 4px;
    cursor: pointer;
    color: #838383;
    font-size: 1rem;
  }

  .show-comments {
    display: none;
  }

  .comment-show-box {
    background-color: #f2f3f5;
    border-radius: 18px;
    box-sizing: border-box;
    color: #1c1e21;
    display: inline-block;
    line-height: 16px;
    width: 100%;
    padding: 7px 18px 3px 15px;
    margin: 0;
    max-width: 100%;
    word-wrap: break-word;
    position: relative;
    white-space: normal;
    word-break: break-word;
  }

  .time_ago {
    font-size: 0.5rem;
    margin-right: 12px;
    margin-bottom: 3px;
  }




  .lds-ripple {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }

  .lds-ripple div {
    position: absolute;
    border: 4px solid #13924A;
    opacity: 1;
    border-radius: 50%;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }

  .lds-ripple div:nth-child(2) {
    animation-delay: -0.5s;
  }

  @keyframes lds-ripple {
    0% {
      top: 36px;
      left: 36px;
      width: 0;
      height: 0;
      opacity: 1;
    }

    100% {
      top: 0px;
      left: 0px;
      width: 72px;
      height: 72px;
      opacity: 0;
    }
  }
</style>

<div class=" container text-center mt-3 postlist">




</div>
<div class="text-center">
  <div class="lds-ripple">
    <div></div>
    <div></div>
  </div>
</div>

<input type="hidden" name="community_page" id="community_page" value="1">

<!-- <footer style="background: black; width: 100%; height: 60px; position: absolute; left: 0; right: 0"></footer> -->