<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-IIED/eyOkM6ihtOiQsX2zizxFBphgnv1zbe1bKA+njdFzkr6cDNy16jfIKWu4FNH" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/ranking.css">
  <link rel="stylesheet" href="./css/detail.css">
  <title>Bienvenido</title>
</head>
<body>
  <?php include('./include/menu.php') ?>
  <!-- PageContent -->
  <div class="container">
    <div class="row justify-content-center">
      <div class="card mt-4 col-12 col-lg-8 pl-5 pr-5 pb-6">
        <h5 class="text-center mt-4 mb-4" >Ranking</h5>
        <p class="text-center" style="font-size: 14px">
        Aquí podrás ver tus puntos acumulados y los de toda la comunidad.
        </p>
        <div class="ranking__container mt-2">
            <?php include('include/rankingList.php');?>
        </div>
        <div class="text-center" style="">
          <i class="fal fa-angle-down" style="color: #707070; font-size: 40px"></i>
        </div>
      </div>
    </div>
  </div>
  <!-- /PageContent -->

  <div class="modal" tabindex="-1" role="dialog" id="modalauto">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content p-3">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 20px; right: 20px; z-index: 99999">
          <div class="fal fa-times" style="font-size: 38px; color: #707070;"></div>
        </button>
          <div class="owl-carousel owl-in-modal">
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">RANKING</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Aquí podrás ver tu posición en el ranking, tus puntos acumulados y los de toda la comunidad. 
                </p>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-12">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">Recuerda que para ganar puntos debes:</div>
                <div class="poihts_container d-flex align-items-center">
                  <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 5 Pts</div>
                  <div style="color: #333333; font-size: 14px">Participando en la actividad del día y compartiendo tu foto una única vez, obtienes 5.</div>
                </div>
                <div class="poihts_container d-flex align-items-center">
                  <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 3 Pts</div>
                  <div style="color: #333333; font-size: 14px">Recibiendo comentarios o comentando las fotos de los otros niños  obtienes 3 puntos.</div>
                </div>
                <div class="poihts_container d-flex align-items-center">
                  <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 1 Pts</div>
                  <div style="color: #333333; font-size: 14px">Recibiendo me gusta en tus publicaciones o dando me gusta en las publicaciones de los otros niños obtienes 1 punto.</div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"  crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <?php include('me.php');?>
  <script>
        var token = localStorage.getItem('token');
        var son_id = localStorage.getItem('son_id');

        if (!token) {
            location.href = '/'
        }else{
          if(!son_id){
              location.href = '/'
          } 
        }    

        $(document).ready(function(){
          $('#modalauto').modal('toggle');
          $(".owl-carousel").owlCarousel({
            items: 1,
            dots: true,
            nav: true
          });
        });
        
        $("#clear_sons").on("click", function(){
          localStorage.removeItem('son_id');
          location.href = '/'
        });
  
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>
</html>