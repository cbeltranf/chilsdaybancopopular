<?php
  include('./include/utils.php');
  $id = $_GET['id'];
  $result = callAPI('get', 'https://api.siempreconectados.com/api/activities/'. $id);
  $item = json_decode($result);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-IIED/eyOkM6ihtOiQsX2zizxFBphgnv1zbe1bKA+njdFzkr6cDNy16jfIKWu4FNH" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <link rel="stylesheet" href="./css/detail.css">
  <title>Bienvenido</title>
</head>
<body>
  <?php include('./include/menu.php') ?>
  <!-- PageContent -->
  <div class="container">
    <a href="/bienvenido.php" class="d-flex align-items-center my-4 btn btn-clear" style="color: #009F4D; font-size: 14px; margin-left: -10px"><i class="fa fa-angle-left" style="font-size: 30px; margin-right: 5px"></i> Volver</a>
    <div style="background-color: #F4F4F4; position: relative" class="py-3 px-4">
      <div style="position: absolute; top: 5px; left: 5px" class="d-flex align-items-center">
        <img src="https://api.siempreconectados.com/media/category/<?= $item->category->photo ?>" style="" width="45" />
        <div>
          <div style="background-color: <?= $item->category->color ?> ; color: white; font-size: 19px; border-radius: 17px" class="ml-3 py-1 px-5"><?= $item->category->name ?></div>
          <div style="margin-left: 20px; font-size: 14px; margin-top: 5px;color: rgba(0,0,0,0.6)"><?= $item->tittle ?></div>
        </div>
      </div>
      <div class="owl-carousel carousel walkthrought">
        <?php foreach($item->steps as $step) {?>
          <div class="tutstep__container">
            <div class="tutstep__img" style="background-image: url(<?= $step->image ?>"></div>
            <div class="tutstep__title mt-2 mb-1"><?= $step->title ?></div>
            <div class="tutstep__desc"><?= $step->description ?></div>
          </div>
        <?php }?>
      </div>
    </div>
    <!-- Final instructivo -->
    <div class="my-5">
      <!-- <h5 class="text-strong"><?= $item->tittle ?></h5> -->
      <p class="infop"><?= $item->content ?></p>
    </div>
    <div class="my-5">
      <h5 class="text-strong mb-3">PUNTOS</h5>
      <div class="poihts_container d-flex align-items-center">
        <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 5 Pts</div>
        <div style="color: #333333">Participando en la actividad del día y compartiendo tu foto una única vez, obtienes 5.</div>
      </div>
      <div class="poihts_container d-flex align-items-center">
        <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 3 Pts</div>
        <div style="color: #333333">Recibiendo comentarios o comentando las fotos de los otros niños  obtienes 3 puntos.</div>
      </div>
      <div class="poihts_container d-flex align-items-center">
        <div class="points__count text-secondary mr-4 text-strong"><fa class="fa fa-trophy-alt"></fa> 1 Pts</div>
        <div style="color: #333333">Recibiendo me gusta en tus publicaciones o dando me gusta en las publicaciones de los otros niños obtienes 1 punto.</div>
      </div>
    </div>
    <div class="my-5">
      <h5 class="text-strong">ENVÍO DE FOTO PARA PARTICIPAR</h5>
      <input type="file" name="file" id="file" class="form-control" hidden accept="image/*">
      <button  class="btn btn-primary" style="padding-left: 30px; padding-right: 30px" id="Btn_solver">Subir foto</button>      
      <button  class="btn btn-primary" style="padding-left: 30px; padding-right: 30px; display:none" id="btn_dummy"><i class="fas fa-cloud-upload-alt"></i> Subiendo foto...</button>
      
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="modalauto">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content p-3">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 20px; right: 20px; z-index: 99999">
          <div class="fal fa-times" style="font-size: 38px; color: #707070;"></div>
        </button>
          <div class="owl-carousel owl-in-modal detail-carousel">
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">COMENZAR ACTIVIDAD</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Aquí te contaremos las instrucciones de lo que debes hacer en cada actividad, paso a paso.
                </p>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  ¡Anímate a participar activamente!
                </p>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">SUBIR FOTOS</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  Cuando realices el reto, toma una foto con ayuda de tus papás, y súbela a la página dando clic en el botón 
                </p>
                <div style="background-color: linear-gradient(to bottom, #1E9E3E 50%, #037F5E); color: white; width: 100%; padding: 12px 0" class="mock-btn">SUBIR FOTO</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  ¡Y así empezarás a ganar puntos!
                </p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  <!-- /PageContent -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"  crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/javascript-canvas-to-blob/3.20.0/js/canvas-to-blob.js"></script>
  <?php include('me.php');?>
  <script>
      function resizeImage(input, onBlob)
      {
          var filesToUpload = input.files;
          var file = filesToUpload[0];


          // Create an image
          var img = document.createElement("img");
          // Create a file reader
          var reader = new FileReader();
          // Set the image once loaded into file reader
          reader.onload = function(e)
          {
              img.src = e.target.result;
              img.onload = function() {
                var canvas = document.createElement("canvas");
                //var canvas = $("<canvas>", {"id":"testing"})[0];
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 800;
                var MAX_HEIGHT = 600;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                  if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                  }
                } else {
                  if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                  }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                canvas.toBlob(function(blob) {
                  onBlob(blob)
                });
              }

          }
          // Load files into file reader
          reader.readAsDataURL(file);
      }
        // End resize
        $(document).ready(function(){
          $('#modalauto').modal('toggle');
          $(".detail-carousel").owlCarousel({
            items: 1,
            dots: true,
            nav: true
          });
        });
        var token = localStorage.getItem('token');
        var son_id = localStorage.getItem('son_id');
        var act = localStorage.getItem('activity_id');

        $('#Btn_solver').on('click', () => {
          $("#file").click()  
        });

        $("#file").on("change", function() {
          resizeImage(document.getElementById('file'), function(blob)  {
            var formData = new FormData();
            var title = `<?= $item->tittle ?>`;
            title.replace( /[\r\n]+/gm, "" )
            formData.append('file', blob);
            formData.append('activity_id', '<?= $_GET['id'] ?>');
            formData.append('son_id', son_id);
            formData.append('comment', title);


            $.ajax({
              type: 'POST',
              url: 'https://api.siempreconectados.com/api/activitiessoles',
              data: formData,
              contentType: false,
              processData: false,
              beforeSend: function(xhr) {
                var tok = localStorage.getItem('token');
                xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
                $("#btn_dummy").show();
                $('#Btn_solver').hide();
              },
              success: function(msg) {
                $("#btn_dummy").hide();
                $('#Btn_solver').show();
                Swal.fire({
                  icon: 'success',
                  title: 'Éxito',
                  text: 'Publicación realizada con éxito'
                })
              },
              error: function(jqXHR) {
                $("#btn_dummy").hide();
                $('#Btn_solver').show();
                console.log(jqXHR.responseJSON, jqXHR.status);
                response = "Ha ocurrido un error, intenta nuevamente más tarde";
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: response
                })

                return false;
              }
            });
          });
      });

        if (!token) {
            location.href = '/'
        }else{
          if(!son_id){
              location.href = '/'
          } 
        }    
        
        $("#clear_sons").on("click", function(){
          localStorage.removeItem('son_id');
          location.href = '/'
        });

 $( document ).ready(function() {
      $(".walkthrought").owlCarousel({
        margin: 20,
        responsive : {
            // breakpoint from 0 up
            0 : {
              items: 1,
              dots: true,
            },
            // breakpoint from 768 up
            768 : {
              items: 3,
              dots: false,
              nav: true,
            }
        }
      });
    });
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>
</html>