<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-IIED/eyOkM6ihtOiQsX2zizxFBphgnv1zbe1bKA+njdFzkr6cDNy16jfIKWu4FNH" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <link rel="stylesheet" href="./css/lastActivity.css">
  <link rel="stylesheet" href="./css/aCalendar.css">
  <title>Bienvenido</title>
</head>

<body>
  <?php include('./include/menu.php') ?>
  <!-- PageContent -->
  <div>
    <?php include('./include/communityPostCreate.php') ?>
    <?php include('./include/communityPostList.php') ?>
  </div>
  <!-- /PageContent -->

  <div class="modal" tabindex="-1" role="dialog" id="modalauto">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content p-3">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 20px; right: 20px; z-index: 99999">
            <div class="fal fa-times" style="font-size: 38px; color: #707070;"></div>
          </button>
            <div class="row justify-content-center">
              <div class="col-8">
                <img src="/assets/children.png" class="w-100" />
                <div class="tour-modal-title">COMUNIDAD</div>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  En está sección, también podrás publicar tus fotos de las actividades, ver las fotos que los otros niños publican, comentar y dar me gusta .
                </p>
                <p class="text-center" style="font-size: 14px; color: #000000">
                  ¡Acumula más y más puntos!
                </p>
              </div>
            </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <?php include('me.php');?>
  <script>
    var token = localStorage.getItem('token');
    if (!token) {
      location.href = '/'
    }


    $(document).ready(function(){
      $('#modalauto').modal('toggle');
    });

    $(document).ready(function() {

      $(".showcomments").on("click", function() {
        var id = $(this).attr("data-id");
        console.log(id);
        $(".show-comments" + id).fadeToggle();
      });

      $("#add_photo").on("click", function() {
        $("#post_image").click();
      });

      $("#send_post").on("click", function() {
        if ($("#post_image").val() == '') {
          response = "Debes seleccionar una imagen";
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: response
          })
          return false;
        }

        if ($("#newpost").val() == '') {
          response = "Ingresa tu comentario sobre la foto";
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: response
          })
          return false;
        }


        var son = localStorage.getItem('son_id');
        var act = localStorage.getItem('activity_id');
        var formData = new FormData();
        formData.append('comment', $('#newpost').val());
        formData.append('file', $('input[type=file]')[0].files[0]);
        formData.append('activity_id', act);
        formData.append('son_id', son);

        $.ajax({
          type: 'POST',
          url: 'https://api.siempreconectados.com/api/activitiessoles',
          data: formData,
          contentType: false,
          processData: false,
          beforeSend: function(xhr) {
            var tok = localStorage.getItem('token');
            xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
            $("#send_post").hide();
            $("#add_photo").hide();
          },
          success: function(msg) {
            dinamic_new_post(msg.data, 'prepend');

            Swal.fire({
              icon: 'success',
              title: 'Éxito',
              text: 'Publicación realizada con éxito'
            })

            $("#send_post").show();
            $("#add_photo").show();
            $("#newpostform")[0].reset();
          },
          error: function(jqXHR) {
            console.log(jqXHR.responseJSON, jqXHR.status);
            response = "Ha ocurrido un error, intenta nuevamente más tarde";
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: response
            })
            $("#send_post").show();
            $("#add_photo").show();
            return false;
          }
        });


      });


      flag = true;
      $(window).scroll(function() {
        //console.log("in", $(window).height(), $(document).height(), $(window).scrollTop());
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
          //console.log("enter");
          page = parseInt($("#community_page").val()) + 1;
          no_data = true;
          if (flag && no_data) {            
            get_page(page)
          }
        }
      });

      get_page(1);

      function get_page(page) {
        $("#community_page").val(page);
        $.ajax({
          type: 'GET',
          url: 'https://api.siempreconectados.com/api/activitiessoles?page=' + page,
          beforeSend: function(xhr) {
            var tok = localStorage.getItem('token');
            xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
            xhr.setRequestHeader('Content-Type', 'application/json');
            $(".lds-ripple").show();
          },
          success: function(msg) {
            if(msg.data.length<1){
              flag = false; //flag scroll
            }
            $.each(msg.data, function(index, value) {
              dinamic_new_post(value, 'append');
            });

            /*Swal.fire({
              icon: 'success',
              title: 'Éxito',
              text: 'Publicación realizada con éxito'
            })*/


            $(".lds-ripple").fadeOut();
          },
          error: function(jqXHR) {
            console.log(jqXHR.responseJSON, jqXHR.status);
            response = "Ha ocurrido un error, intenta nuevamente mas tarde";
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: response
            })
            $(".lds-ripple").fadeOut();
            return false;
          }
        });
      }

      function dinamic_new_post(data, type_insertion) {

        var base = localStorage.getItem('base_url');

        var header = $('<div>').attr({
          class: "card-header text-left"
        }).append(
          $('<span>', {
            html: `
              <img src="//api.siempreconectados.com/media/${data.son.avatar}" style="width: 30px; height: 30px; border-radius: 50%; margin-right: 10px; border: 0.5px solid rgba(0,0,0,0.2)" /></img>
              <span class: "badge badge-success my-badge">${data.son.name}</span>
            `
          })
          /* .attr({
            
          }) */
        );

        var ext = data.file.split('.');
        if(ext[ext.length-1] == 'mp4'){
        var image = $('<video>').attr({
          'width': '100%',
          class: "card-img-top",
          "controls":true,
        }).append(
          '<source src="'+base + '/storage/' + data.file+ '" type="video/mp4">'
          );
      
        }else{
        var image = $('<img>').attr({
          class: "card-img-top",
          src: base + '/storage/' + data.file, //'https://inmov.info/img/image_post.png', //base + '/media/' + data.file,
        })
        }


        var publish_desc ='';
        var mm = '';
        if(data.comment){
          publish_desc = $('<span>',{
            html: data.comment 
          })

          mm = "mb-2"

        }

        var body_row_0 = $('<div>').attr({
          class: "row"
        }).append(
          $('<div>').attr({
            class: "col-12 "+mm
          }).append(
            publish_desc
          )
        );

        if(data.meLike == true){
          var tp = 'fas'
        }else{
          var tp = 'far'
        }
        var body_row_1 = $('<div>').attr({
          class: "row"
        }).append(
          $('<div>').attr({
            class: "col-12 interactions"
          }).append(
            $('<i>').attr({
              class: tp+" fa-heart golike ilikethis" + data.id,
              'data-id': data.id
            }).on('click', function() {
              var id = $(this).attr("data-id");
              like_this(id);
            }),
            $('<i>').attr({
              class: "far fa-comment gocomment",
              'data-id': data.id
            }).on('click', function() {
              var id = $(this).attr("data-id");
              $(".newcomment" + id).focus();
            }),
          )
        );

        var body_row_2 = $('<div>').attr({
          class: "row"
        }).append(
          $('<div>', {
            html: ' Me gusta'
          }).attr({
            class: "col-12 mt-1"
          }).prepend(
            $('<span>', {
              html: data.count_likes
            }).attr({
              class: "likecant font-weight-bold likecant"+data.id,
              'data-id': data.id
            })
          )
        );


        var body_row_3 = $('<div>').attr({
          class: "row"
        }).append(
          $('<div>').attr({
            class: "col-12"
          }).append(

            $('<a>').attr({
              class: "showcomments",
              'data-id': data.id,
              href: 'javascript:void(0)'
            }).on("click", function() {
              var id = $(this).attr("data-id");
              $(".show-comments" + id).fadeToggle();
            }).append(
              (data.comments.length == 1) ? 'Ver ' : 'Ver los ',
              $('<span>', {
                html: (data.comments.length == 0 || data.comments.length == 1) ? '' : data.comments.length
              }).attr({
                class: "commentcant",
                'data-id': data.id
              }),
              ' comentarios'

            )
          )
        );

        var body_row_4 = $('<div>').attr({
          class: "row"
        }).append(
          $('<div>').attr({
            class: "col-12 leavecomments"
          }).append(

            $('<div>').attr({
              class: "feedback__btn"
            }).append(
              $('<i>').attr({
                class: "fas fa-paper-plane  mt-2  sendcomment",
                'data-id': data.id
              }).on("click", function() {
                var element = $(this);
                var id = $(this).attr("data-id");
                $(this).hide();
                $(".show-comments" + id).fadeIn();
                var comment = $(".newcomment" + id).val();
                publish_comment(id, comment, element);
              }),
            ),
            $('<textarea>').attr({
              class: "newcomment mt-1 newcomment" + data.id,
              'data-id': data.id,
              placeholder: "Agrega un comentario"
            })

          )
        );



        var comments_box = $('<div>').attr({
          class: "how-comments show-comments" + data.id + " mt-1",
          'data-id': data.id,
        }).css("display", "none");

        $.each(data.comments, function(index, value) {
          // alert(index + ": " + value);

          show_comment(comments_box, value)


        });

        var body_comments = comments_box;



        var body = $('<div>').attr({
          class: "card-body text-left  pt-1"
        }).append(
          body_row_0,
          body_row_1,
          body_row_2,
          body_row_3,
          body_row_4,
          body_comments
        );

        var new_post = $("<div>").attr({
          class: "row justify-content-md-center mb-3"
        }).append(
          $('<div>').attr({
            class: "col-md-8 col-sm-12"
          }).append(
            $('<div>').attr({
              class: "card my-card shadow-sm"
            }).append(
              header,
              image,
              body
            )
          )
        );

        if(type_insertion=='prepend'){
          //console.log("pre");
        $(".postlist").prepend(new_post);
        }else{
          //console.log("app");
        $(".postlist").append(new_post);
        }


      }

      function show_comment(comments_box, value) {
        comments_box.append(
          $('<div>').attr({
            class: "col-md-11 col-sm-12",
          }).append(
            $('<div>').attr({
              class: "comment-show-box",
            }).append(
              $('<label>', {
                html: '<strong>' +
                 value.son.name + ':</strong> ' + value.comment
              }).attr({
                class: "font-weight-bold mr-1",
              }),
            ),
            $('<div>', {
              html: value.created_at
            }).attr({
              class: "text-right time_ago",
            })

          ),

        );
      }

      function publish_comment(id, comment, element) {

        var son = localStorage.getItem('son_id');
        var act = localStorage.getItem('activity_id');

        var formData = new FormData();
        formData.append('comment', comment);
        formData.append('activity_solve_id', id);
        formData.append('son_id', son);
        $.ajax({
          type: 'POST',
          url: 'https://api.siempreconectados.com/api/comments',
          data: formData,
          contentType: false,
          processData: false,
          beforeSend: function(xhr) {
            var tok = localStorage.getItem('token');
            xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
          },
          success: function(msg) {

            //show_comment(comments_box, value)

            element.show();

            $(".show-comments" + id).append(
              $('<div>').attr({
                class: "col-md-11 col-sm-12",
              }).append(
                $('<div>').attr({
                  class: "comment-show-box",
                }).append(
                  $('<label>', {
                    html: '<strong>' + msg.data.son.name + ':</strong> ' + msg.data.comment
                  }).attr({
                    class: "font-weight-bold mr-1",
                  }),


                ),
                $('<div>', {
                  html: 'Hace unos segundos'
                }).attr({
                  class: "text-right time_ago",
                })

              ),

            );

          },
          error: function(jqXHR) {
            console.log(jqXHR.responseJSON, jqXHR.status);
            response = "Ha ocurrido un error, intenta nuevamente mas tarde";
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: response
            })
            return false;
          }
        });
      }

      function like_this(id) {

        var son = localStorage.getItem('son_id');
        var act = localStorage.getItem('activity_id');

        var formData = new FormData();

        formData.append('activity_solve_id', id);
        formData.append('son_id', son);
        $.ajax({
          type: 'POST',
          url: 'https://api.siempreconectados.com/api/likes',
          data: formData,
          contentType: false,
          processData: false,
          beforeSend: function(xhr) {
            var tok = localStorage.getItem('token');
            xhr.setRequestHeader('Authorization', 'Bearer ' + tok);
          },
          success: function(msg) {

            //show_comment(comments_box, value)
            $(".ilikethis" + id).removeClass('far').addClass('fas');

            $(".likecant"+id).text(parseInt($(".likecant"+id).text())+1);


          },
          error: function(jqXHR) {
            console.log(jqXHR.responseJSON, jqXHR.status);
            if (jqXHR.status == 500) {
              $(".ilikethis" + id).removeClass('far').addClass('fas');
            } else {
              response = "Ha ocurrido un error, intenta nuevamente más tarde";
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: response
              })
            }


            return false;
          }
        });
      }


    }); //DR
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>

</html>