<?php
  /* ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL); */
  include('./include/utils.php');
  $result = callAPI('get', 'https://api.siempreconectados.com/api/sons');
  $sons = json_decode($result);
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/theme.css">
  <link rel="stylesheet" href="./css/sons.css">
  <title>Dia del niño</title>
</head>

<body style="width: 100%; height: 100%; background: url('./assets/login.png'); background-size: cover">
  <img src="./assets/logo_bp.png" style="position: fixed; top: 0; right: 63px" width="270" class="brand_logo" />
  <img src="./assets/logo_ac.png" style="position: fixed; bottom: 130px; left: 0" width="114" class="logo_ac" />
  <form id="sons" class="pt-5 text-center">
    <img src="./assets/children.png" style="margin: 0 auto" class="login-img" />
    <h3 class="text-center" style="color: #595D6E; font-size: 21px; margin-bottom: 34px">¿Quién quiere ingresar ahora?</h3>
    <div class="container">
      <div id="sons-row" class="row justify-content-center align-items-center">
      <?php include("./include/sonsAvatar.php");?>

      </div>
    </div>
  </form>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <?php include('me.php');?>

  <script>
    var token = localStorage.getItem('token');
    if (!token) {
      location.href = '/'
    }

    $("body").on("click", '.btn_sons', function(){
      var son_id = $(this).attr('id');
      localStorage.setItem("son_id", son_id);
      window.location.href = "/"
    });

    try {
      var user = JSON.parse(localStorage.getItem('user'));
    } catch(ex) {
      window.location.href = "/login.php"
    }

  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164684707-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164684707-1');
</script>

</body>

</html>